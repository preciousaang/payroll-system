
import express from 'express'
import './providers/db-provider'
import loadRoutes from './providers/route-provider'
const PORT = process.env.PORT || 3000;
const app = express();
loadRoutes(app)

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
