import { Application } from "express";
import authRouter from '../routes/auth.route'

export default (app: Application) => {
    app.use('/auth', authRouter)
}